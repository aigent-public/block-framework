import {IPayloadType} from "./payload-type";
import {IChannel} from "./channel";
import {IIpv4} from "./IIpv4";
import {IAgentState} from "./agent-state";
import {IEventType} from "./event-type";

export type IMetadata = {
    ssrc?: number;
    payloadType?: IPayloadType;
    len?:number;
    protocol?:number;
    saddr?: IIpv4;
    daddr?: IIpv4;
    sport?: number;
    dport?: number
    etag?: string;
    filename?: string;
    channel?: IChannel;
    sec?: number;
    usec?: number;
    agent_id?: string;
    sequence_number?: number;
    timestamp?: number;
    csrc?:any[];
    event_type?:IEventType;
    agentState?:IAgentState;
}
