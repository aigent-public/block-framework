import {IMetadata} from "./metadata";
import {IEventTracking} from "./tracking";

export type IMessage = {
    eventTracking?: IEventTracking;
    metadata?: IMetadata;
    payload?: any;
    debug?: any;
}