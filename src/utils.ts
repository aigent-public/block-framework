import {IMessage} from "./message";
import {IEventTracking} from "./tracking";
import {v4} from 'uuid';

export class Utils {
    static getStreamID(data: IMessage): string {
        return `${data.metadata.payloadType.name}_${data.metadata.ssrc}_${data.metadata.channel}`;
    }

    static generateEventTracking = (prev_event: IEventTracking = null, serviceName: string, sourceTopic: string, publishTopic): IEventTracking => {
        const d = new Date();
        return {
            id: v4(),
            serviceName: serviceName,
            tag: prev_event ? prev_event.tag : v4(),
            sourceTopic: sourceTopic,
            publishTopic: publishTopic,
            publishedOn: d,
            receivedOn: d,
            triggeredBy: prev_event
        };
    };
}
