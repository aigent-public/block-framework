"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("uuid");
class Utils {
    static getStreamID(data) {
        return `${data.metadata.payloadType.name}_${data.metadata.ssrc}_${data.metadata.channel}`;
    }
}
Utils.generateEventTracking = (prev_event = null, serviceName, sourceTopic, publishTopic) => {
    const d = new Date();
    return {
        id: uuid_1.v4(),
        serviceName: serviceName,
        tag: prev_event ? prev_event.tag : uuid_1.v4(),
        sourceTopic: sourceTopic,
        publishTopic: publishTopic,
        publishedOn: d,
        receivedOn: d,
        triggeredBy: prev_event
    };
};
exports.Utils = Utils;
