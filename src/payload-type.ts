import {IChannel} from "./channel";

export type IPayloadType = {
    name: string;
    mediaType?: string;
    clockRate?: number;
    channels?: 1 | 2;
}
