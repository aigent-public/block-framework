import {IIpv4} from "./IIpv4";

export type IAgentState = {
    loggedIn?: boolean;
    agentId?: number;
    phoneIp?: IIpv4;
    vdnSkill?:string;
    ani?: string;
    agentAni?: string;
}

