type UUID = string;

export type IEventTracking = {
    id: UUID;
    serviceName: string;
    tag: UUID;
    sourceTopic?: string;
    publishTopic: string;
    publishedOn: Date;
    receivedOn?: Date;
    triggeredBy?: IEventTracking;
}
